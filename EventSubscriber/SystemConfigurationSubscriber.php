<?php

/*
 * This file is part of the "GitLabBundle" for Kimai 2.
 * All rights reserved by Kevin Papst (www.kevinpapst.de).
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace KimaiPlugin\GitLabBundle\EventSubscriber;

use App\Event\SystemConfigurationEvent;
use App\Form\Model\Configuration;
use App\Form\Model\SystemConfiguration;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class SystemConfigurationSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            SystemConfigurationEvent::class => ['onSystemConfiguration', 100],
        ];
    }

    public function onSystemConfiguration(SystemConfigurationEvent $event): void
    {
        $event->addConfiguration(
            (new SystemConfiguration('gitlab'))
            ->setConfiguration([
                (new Configuration('gitlab_private_token'))
                    ->setLabel('gitlab.private_token')
                    ->setOptions(['help' => 'help.gitlab.private_token'])
                    ->setTranslationDomain('system-configuration')
                    ->setType(TextType::class),
                (new Configuration('gitlab_instance_base_url'))
                    ->setLabel('gitlab.instance_base_url')
                    ->setTranslationDomain('system-configuration')
                    ->setType(UrlType::class),
            ])
        );
    }
}